<?php
header('Content-Type: text/html; charset=utf-8');
session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL | E_STRICT);

define('PROJECT_DIR', realpath('./'));
if($_SERVER['SERVER_NAME']=='localhost')
{
	define('HOST','http://localhost:8080/mordus/v5/');
}else{
	define('HOST','http://v5.lesmordusduclavier.fr/');
}
define("FRONT_CONTROLER", "Yes I'm coming from front controler !");
define("MODEL_DIR", PROJECT_DIR."/model/");
define("CLASS_DIR", PROJECT_DIR."/model/classes/");
define("ENUM_DIR", PROJECT_DIR."/model/classes/enums/");
define("EXCEPTION_DIR", PROJECT_DIR."/model/classes/exception/");
define("CONTROLER_DIR", PROJECT_DIR."/controler/");
define("TPL_DIR", PROJECT_DIR."/templates/");
define("UPLOAD_DIR", PROJECT_DIR."/ressources/images/uploads/");
define("LIB_DIR", PROJECT_DIR."/lib/");

define('LOCALE_DIR', PROJECT_DIR.'/locale');
define('DEFAULT_LOCALE', 'fr_FR');

require_once EXCEPTION_DIR.'FrontControlerException.class.php';
require_once EXCEPTION_DIR.'NotSupportedMethodException.class.php';

require_once ENUM_DIR.'AccessLevel.enum.php';
require_once ENUM_DIR.'TypesMessages.enum.php';

require_once CLASS_DIR.'Database.class.php';
require_once CLASS_DIR.'DBH.class.php';
require_once CLASS_DIR.'smtp.class.php';
require_once CLASS_DIR.'phpmailer.class.php';
require_once CLASS_DIR.'Notif.class.php';
require_once CLASS_DIR.'Object.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'Category.class.php';
require_once CLASS_DIR.'Forum.class.php';
require_once CLASS_DIR.'Message.class.php';
require_once CLASS_DIR.'Parsedown.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'Upload.class.php';
require_once CLASS_DIR.'Templates.class.php';

require_once MODEL_DIR.'functions.php';

require_once LIB_DIR.'gettext/gettext.inc';

$supported_locales = array('fr_FR');
$encoding = 'UTF-8';

if(isset($_GET['lang']))
{
	$locale = $_GET['lang'];
	setcookie("lang",$locale);
}else if(isset($_COOKIE['lang'])){
	$locale = $_COOKIE['lang'];
}else{
	$locale = DEFAULT_LOCALE;
}
//$locale = (isset($_GET['lang']))? $_GET['lang'] : DEFAULT_LOCALE;

// gettext setup
T_setlocale(LC_MESSAGES, $locale.'.utf8');
// Set the text domain as 'messages'
$domain = 'default';
bindtextdomain($domain, LOCALE_DIR);
// bind_textdomain_codeset is supported only in PHP 4.2.0+
if (function_exists('bind_textdomain_codeset')) 
  bind_textdomain_codeset($domain, $encoding);
textdomain($domain);
$tpl = new Templates;
$Parsedown = new Parsedown();
$tpl->DEBUG = false; // Pour afficher les {{message}} qui ne sont pas remplacés par le code php, changer en true
$page = '';
$_links = array(); $_links[] = array('link' => 'accueil', 'txt' => 'Accueil');
	$tpl->value('host',HOST);

if(!empty($_GET['ajax'])){
	switch ($_GET['ajax']) {
		case 'previewMarkdown':
				if(isset($_POST['text']) && !empty($_POST['user'])){
						$user = DBH::getUnique('User',array('id'=>$_POST['user']));
						if(!empty($user)){
						$tpl->value('text',$Parsedown->text($_POST['text']));
						$tpl->value('author',$user->getPseudo());
						$tpl->value('userPicture',$user->getPicture());
						$tpl->value('signature',$user->getSignature());
						$tpl->value('date',date("Y-m-d H:i:s"));
						
						echo $tpl->build('messages/message');					
						}else{
							echo "Mauvais identifiant utilisateur";
						}
				}else{
					echo "Paramètres invalides";
				}
			break;
		
		default:
			# code...
			break;
	}
}else{
	$tpl->value('imageLeft',HOST.'files/gallery/cotes-gauche-v2.png');
	$tpl->value('imageRight',HOST.'files/gallery/cotes-droit.png');
	include(CONTROLER_DIR."header.php");
	if (isset($_GET['op'])&&(file_exists(CONTROLER_DIR.$_GET['op'].'.php')))
	{
		switch ($_GET['op']) {
			case 'accueil':
					$tpl->value('activeAccueil','active');
				break;
			case 'forum':
					$tpl->value('activeForum','active');
				break;
		}
		include(CONTROLER_DIR.$_GET['op'].'.php');
	}
	else
	{
		$tpl->value('activeAccueil','active');
		include(CONTROLER_DIR.'accueil.php');
	}
	
	$tpl->value('lang', $locale);
	$tpl->value('titre', "Les Mordus du  clavier");
	$tpl->value('notifs',Notif::display($tpl));
	$tpl->value('content',$page);
	$tpl->value('breadcrumb',breadcrumb($_links));
$generatedPage = $tpl->build('principal');

echo $generatedPage;
}
/*
select N.nid as id,
F.uri as image,
1 as type,
B.body_value as text,
FROM_UNIXTIME(N.created) as date,
1 as author,
N.title as titre,
null as linkedTo
from mdc_node N, mdc_field_data_body B, mdc_field_data_field_image I, mdc_file_managed F 
where N.type='article' and B.entity_id=N.nid and I.entity_id=N.nid and I.field_image_fid=F.fid
*/