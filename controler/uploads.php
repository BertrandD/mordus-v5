<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
if(!empty($user))
{
	$uploads = DBH::getList('Upload',array('user' => $user->getId()));
	$tpl->getBoucle('user/uploads');
	foreach ($uploads as $upload) {
		if (!file_exists($upload->getThumb()))
		{
			make_thumb("files/".$upload->getFolder(),$upload->getFile(),$upload->getThumb(),250);
		}
		$tpl->value('link',HOST.$upload->getPath());
		$tpl->value('thumb',HOST.$upload->getThumb());
		$tpl->boucle();
	}
	$page.=$tpl->finboucle();
}
