<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

session_destroy();
$user=null;

Notif::add('success',_('Vous avez bien été déconnecté, a bientôt !'));

$page.=$tpl->build('user/logout');
$_links[]=array('txt' => 'déconnexion');