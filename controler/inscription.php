<?php
/**
* @author Leboc Philippe
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(isset($_POST['inscription']))
{
	extract($_POST);
	if(!validate_string($pseudo))
	{
		$erreurs[] = _("Merci d'indiquer votre nom (de 2 à 16 caractères)");
	}
	if(empty($password))
	{
		// pas de check sur la longueur du password du fait qu'il
		// sera hash avant d'arriver ici (javascript) il aura donc
		// forcément une certaine longueur.
		$erreurs[] = _("Merci d'indiquer un mot de passe");
	}
	if(empty($password2))
	{
		$erreurs[] = _("Merci de confirmer le mot de passe");
	}
	if(!validate_mail($mail))
	{
		$erreurs[] = _("Merci d'indiquer une adresse mail valide : client@exemple.com");
	}
	if(!validate_mail($mail2))
	{
		$erreurs[] = _("Merci de confirmer l'adresse mail valide : client@exemple.com");
	}

	if(empty($erreurs))
	{
		if($password !== $password2)
		{
			$erreurs[] = _("Les deux mots de passe ne concordent pas");
		}
		if($mail !== $mail2)
		{
			$erreurs[] = _("Les adresses mails ne concordent pas");
		}

		$id = array("mail" => $mail);
		$user = DBH::getUnique('User',$id);

		if(!empty($user))
		{
			$erreurs[] = _("Cette adresse mail est déjà utilisée");
		}
		$user = NULL;
	}

	if(empty($erreurs))
	{
		$user = DBH::create('User',$_POST);
		DBH::save($user);
		$user = DBH::getUnique('User',array(
			"mail" => $mail
			));
	}

	foreach ($_POST as $key => $value) {
		$tpl->value($key,$value);
	}
}

if(!empty($erreurs))
{
	Notif::add('error',$erreurs);
}

if(empty($user))
{
	$tpl->value('formulaire',$tpl->build('authentification/formulaire_inscription'));
	$tpl->value('boutonCommencer','');
}else{
	$_SESSION['id']=$user->getId();
	Notif::add('success',_('Bienvenue').' '.$user->getPseudo().' '._('chez les mordus !'));
}

$page.=$tpl->build('authentification/inscription');
$_links[]=array('txt' => 'inscription');