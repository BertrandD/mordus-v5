<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
$_links[]=array('txt' => 'contact');
// Check for empty fields
if(!empty($_POST['name'])      &&
   !empty($_POST['email'])     &&
   !empty($_POST['message'])   &&
   filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	$name = $_POST['name'];
	$email_address = $_POST['email'];
	$message = $_POST['message'];

	$mail = new PHPMailer;

	//$mail->SMTPDebug = 3;                               // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'contact@lesmordusduclavier.fr';                 // SMTP username
	$mail->Password = 'Mordus070214';                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->From = 'noreply@lesmordusduclavier.fr';
	$mail->FromName = 'Les Mordus du Clavier';
	$mail->addAddress('contact@lesmordusduclavier.fr', 'Les Mordus du Clavier');     // Add a recipient
	$mail->addBCC('ichiyose@lesmordusduclavier.fr', $name);     // Add a recipient
	$mail->addBCC('shellbash@lesmordusduclavier.fr', $name);     // Add a recipient
	$mail->addReplyTo($email_address, $name);

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = '[Les Mordus du Clavier] Contact à partir du site web';
	$mail->Body    = "Pseudo: $name<br/>Email: $email_address<br/>Message:<br/>$message";
	$mail->AltBody = "Pseudo: $name\n\nEmail: $email_address\n\nMessage:\n$message";

	if(!$mail->send()) {
			Notif::add('error',"Le message n'a pas pu être envoyé : ".$mail->ErrorInfo);
	} else {
			Notif::add('success','Le message a bien été envoyé. Nous vous contacterons au plus tôt ! Merci !');
	}

}

if(!empty($user))
{
	$tpl->value('mail',$user->getMail());
	$tpl->value('pseudo',$user->getPseudo());
	$tpl->value('disabled','readonly');
}else{
	$tpl->value('mail','');
	$tpl->value('pseudo','');
	$tpl->value('disabled','');
}
$page.=$tpl->build('pages/contact');
