<?php
/**
* @author Darbon Bertrand
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($user) && $user->getAccess_level() >= AccessLevel::AUTHOR)
{

	if(isset($_POST['add_article']))
	{
		extract($_POST);
		
		if(empty($titre))
		{
			// pas de check sur la longueur du password du fait qu'il
			// sera hash avant d'arriver ici (javascript) il aura donc
			// forcément une certaine longueur.
			$erreurs[] = _("Merci d'indiquer un titre");
		}
		
		if(empty($text))
		{
			// pas de check sur la longueur du password du fait qu'il
			// sera hash avant d'arriver ici (javascript) il aura donc
			// forcément une certaine longueur.
			$erreurs[] = _("Merci d'écrire un article....");
		}

		if(empty($erreurs))
		{
			if(!empty($_GET['edit']))
			{
				$_POST['id'] = $_GET['edit'];
			}
			$_POST['author'] = $user->getId();
			$_POST['type'] = TypesMessages::ARTICLE;
			$_POST['date'] = date("Y-m-d H:i:s");
			$article = DBH::create('Message',$_POST);
			DBH::save($article);
		}else{
			foreach ($_POST as $key => $value) {
				$tpl->value($key,$value);
			}
		}

	}

	if(isset($_GET['a']))
	{
		$articleEdit = DBH::getUnique('Message',array('type' => TypesMessages::ARTICLE, 'author' => $user->getId(), 'id' => $_GET['a']));
		if(!empty($articleEdit))
		{
			$tpl->value('titre',$articleEdit->getTitre());
			$tpl->value('image',$articleEdit->getImage());
			$tpl->value('text',$articleEdit->getText());
			$tpl->value('edit','/edit/'.$_GET['a']);
		}else{
			$erreurs[]="Impossible d'éditer cet article";
		}
	}

	if(!empty($erreurs))
	{
		Notif::add('error',$erreurs);
	}

	if(empty($article))
	{
		$tpl->value('formulaire',$tpl->build('articles/form_add_article'));
	}else{
		Notif::add('success',_('Félicitation, l\'article a bien été enregistré !'));
	}

	$page.=$tpl->build('articles/add_article');	
	$_links[]=array('txt' => 'ajouter un article');
}
