<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
if(!empty($user))
{
	if(isset($_POST['add_file']))
	{
		if ($_FILES['file']['error'] > 0) 
		{
			switch ($_FILES['file']['error']) {
				case UPLOAD_ERR_NO_FILE:
					$erreurs[]="fichier manquant.";
					break;
				case UPLOAD_ERR_INI_SIZE:
					$erreurs[]="fichier dépassant la taille maximale autorisée par PHP.";
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$erreurs[]="fichier dépassant la taille maximale autorisée par le formulaire.";
					break;
				case UPLOAD_ERR_PARTIAL:
					$erreurs[]="fichier transféré partiellement.";
					break;
				default:
					$erreurs[]="Erreur inconnue";
					break;
			}
		}

		if(empty($erreurs))	
		{
			$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
			$extension_upload = strtolower(  substr(  strrchr($_FILES['file']['name'], '.')  ,1)  );
			if ( !in_array($extension_upload,$extensions_valides) ) $erreurs[]="Extension incorrecte";
		}

		if(empty($erreurs))
		{
			if(empty($_GET['f']) || !is_file('files/'.$_GET['f'])){
				$folder = "gallery";
			}else{
				$folder = $_GET['f'];
			}

			if(!empty($_POST['name']))
			{
				$nom = nettoyerChaine($_POST['name']).'.'.$extension_upload;
			}else{
				$nom = nettoyerChaine($_FILES['file']['name']);
			}
			$out="files/".$folder."/".$nom;
			$res = move_uploaded_file($_FILES['file']['tmp_name'],$out);
			if (!$res) $erreurs[]="Transfert échoué"; else $ok=1;
		}
	}

	if(!empty($erreurs))
	{
		Notif::add('error',$erreurs);
	}else{
		if(!empty($ok))
		{
			Notif::add('success',_('Félicitation, l\'image a bien été envoyée !'));
			$link = "L'adresse de l'image envoyée : <input type='text' onClick='focus();select();' value='".HOST.$out."' />";
			$upload = DBH::create('Upload',array('user' => $user->getId(), 'folder' => $folder, 'file' => $nom, 'date' => date("Y-m-d H:i:s")));
			DBH::save($upload);
		}
	}
	if(!empty($link)){
		$tpl->value('link',$link);
	}

	if(!empty($_GET['f']))
	{
		$tpl->value('folder','/f/'.$_GET['f']);
	}
	$page.=$tpl->build('files/add_file');
	$_links[]=array('txt' => 'ajouter un fichier');
}
