<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
if(!empty($_GET['edit']) && !empty($user))
{
	$message = DBH::getUnique('Message',array('author'=>$user->getId(),'id'=>$_GET['edit']));
	if(!empty($message)){

		if(isset($_POST['add_comment'])){
			extract($_POST);
			
			if(empty($textComment))
			{
				$erreurs[] = _("Merci d'écrire un commentaire....");
			}

			if(empty($erreurs))
			{
				$message->setText($textComment);
				DBH::save($message);
				Notif::add('success','Le message a bien été modifié ! <a href='.$message->getLink().'>Retour sur la page précédante</a>');
			}

		}elseif(isset($_POST['add_sujet'])){
			extract($_POST);
		
			if(empty($textComment))
			{
				$erreurs[] = _("Merci d'écrire un sujet....");
			}
			
			if(empty($titre))
			{
				$erreurs[] = _("Merci de choisir un titre.");
			}

			if(empty($erreurs))
			{
				$message->setTitre($titre);
				$message->setText($textComment);
				DBH::save($message);
				Notif::add('success','Le message a bien été modifié ! <a href='.$message->getLink().'>Retour sur la page précédante</a>');
			}
		}else{
				$tpl->value('titreSujet',$message->getTitre());
				$tpl->value('textComment',$message->getText());
				$tpl->value('from','message/edit/'.$_GET['edit']);
				switch ($message->getType()) {
					case TypesMessages::SUJET:
							$page.=$tpl->build('forums/add_sujet');
						break;
					case TypesMessages::COMMENT:
							$page.=$tpl->build('comments/add_comment');
						break;
					default:
							Notif::add('error','Vous n\'êtes pas sur la bonne page pour éditer ce message');
						break;
				}
		}
	}else{
		Notif::add('error','Le message n\'existe pas ou vous n\'avez pas les droits de le modifier');
	}


}