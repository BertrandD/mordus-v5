<?php

if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($_GET['p']))
{
	$Page = DBH::getUnique('Message', array('id' => $_GET['p'], 'type' => TypesMessages::PAGE));
	$tpl->value('id',$Page->getId());
	if(!empty($Page->getImageLeft())){
		$tpl->value('imageLeft',$Page->getImageLeft());
	}
	if(!empty($Page->getImageRight())){
		$tpl->value('imageRight',$Page->getImageRight());
	}
	$tpl->value('titre',$Page->getTitre());
	$tpl->value('author',$Page->getAuthorObj()->getPseudo());
	if(!empty($user) && $user->getId() == $Page->getAuthor() || $user->getAccess_level() >= AccessLevel::PAGER)
	{
		$tpl->value('edit','<a href="'.HOST.'add_page/a/'.$Page->getId().'">editer</a>');
	}else{
		$tpl->value('edit','');
	}
	$tpl->value('text',$Page->getText());
	$tpl->value('date',$Page->getDate());
	$message=$Page;
	$from="page/a/".$_GET['p'];
	include('add_comment.php');
	include('comments.php');
	$tpl->value('comments',$comments);
	$tpl->value('add_comment',$add_comment);
	$page.=$tpl->build('pages/page');
	$_links[]=array('txt' => 'page');
	$_links[]=array('txt' => $Page->getTitre());
}