<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($_GET['c']))
{
	$confirm = unserialize(base64_decode($_GET['c']));
	if($confirm!=FALSE){
		if(is_array($confirm)){
			$reqUser = DBH::getUnique('User',array('mail' => $confirm['mail'], 'confirmPassword' => $confirm['confirm']));
			if(!empty($reqUser)){
				if(!empty($_POST['password1']) && !empty($_POST['password2']))
				{
					extract($_POST);
					if($password1==$password2){
						$reqUser->setPassword($password1);
						$reqUser->setConfirmPassword('');
						DBH::save($reqUser);
						Notif::add('success','Votre mot de passe a bien été réinitialisé. Vous pouvez désormais vous connecter.');
					}else{
						Notif::add('error','Les mots de passe ne concordent pas');
					}
				}else{
					$tpl->value('titre','Choisissez votre mot de passe');
					$page.=$tpl->build('user/form_changePassword');
				}
				$_links[]=array('txt' => 'mot de passe oublié');

			}else{
				Notif::add('error','Ce lien n\'est pas valide');
			}
		}
	}
}else{
	if(!empty($_POST['mail']))
	{
		$confirm = randomString(50);
		$reqUser = DBH::getUnique('User',array('mail' => $_POST['mail']));
		if(!empty($reqUser))
		{
			$reqUser->setConfirmPassword($confirm);
			DBH::save($reqUser);

			$confirmLink = array('mail' => $_POST['mail'], 'confirm' => $confirm);
			$confirmLink = base64_encode(serialize($confirmLink));

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'ssl0.ovh.net';  	// Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'contact@lesmordusduclavier.fr';                 // SMTP username
			$mail->Password = 'Mordus070214';                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                                    // TCP port to connect to

			$mail->From = 'noreply@lesmordusduclavier.fr';
			$mail->FromName = 'Les Mordus du Clavier';
			$mail->addAddress($_POST['mail']);     // Add a recipient

			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->CharSet = 'UTF-8';
			$mail->Subject = '[Les Mordus du Clavier] Changement de mot de passe';
			$mail->Body    = "Vous avez demandé à pouvoir changer de mot de passe sur le site web des Mordus du Clavier. Si vous n'êtes pas à l'origine de cette requête, ignorez ce mail. Sinon, pour poursuivre votre démarche veuillez cliquer sur ce lien ou le copier/coller dans votre navigateur : <a href='".HOST."password/c/".$confirmLink."'>".HOST."password/c/".$confirmLink."</a><br/><br/>Bonne continuation ! <br/>L'équipe des Mordus du Clavier<br>\n\n<a href='http://lesmordusduclavier.fr'>lesmordusduclavier.fr</a>";

			if(!$mail->send()) {
				Notif::add('error',"Le message n'a pas pu être envoyé : ".$mail->ErrorInfo);
			} else {
				Notif::add('success','Le message a bien été envoyé. Consultez votre boite mail pour continuer.');
			}	
		}else{
			Notif::add('success','Aucun compte lié à cette adresse mail');
		}

	}

	if(empty($user))
	{
		$tpl->value('titre','Demander un nouveau mot de passe');
		$page.=$tpl->build('user/form_reinit');
		$_links[]=array('txt' => 'mot de passe oublié');
	}

}

