<?php
/**
* @author Darbon Bertrand
* @version 1.0
* @require $message de type Message contenant le message (article, discussion, page...) auquel est lié le commentaire
* @require $from de type string contenant l'url réécrite où se situe le formulaire (ex : article/a/4 )
* @return $add_comment -> html généré à placer dans la page avec le formulaire pour ajouter un commentaire
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

$add_comment='';
if(empty($user))
{
	Notif::add('erreur','Vous devez être connecté pour ajouter un commentaire');
}elseif(!empty($message) && !empty($from)){

	if(isset($_POST['add_comment']))
	{
		extract($_POST);
		
		if(empty($textComment))
		{
			// pas de check sur la longueur du password du fait qu'il
			// sera hash avant d'arriver ici (javascript) il aura donc
			// forcément une certaine longueur.
			$erreurs[] = _("Merci d'écrire un commentaire....");
		}

		if(empty($erreurs))
		{
			$a['id'] = DBH::getNextId('Message');
			$a['author'] = $user->getId();
			$a['type'] = TypesMessages::COMMENT;
			$a['date'] = date("Y-m-d H:i:s");
			$a['linkedTo'] = $message->getId();
			$a['text'] = $_POST['textComment'];
			$comment = DBH::create('Message',$a);
			DBH::save($comment);
		}

	}

	if(!empty($erreurs))
	{
			Notif::add('error',$erreurs);
	}

	$tpl->value('from',$from);

	if(empty($comment))
	{
		$tpl->value('formulaire',$tpl->build('comments/add_comment'));
	}else{
		Notif::add('success',_('Félicitation, le commentaire a bien été enregistré !'));
	}

	$add_comment.=$tpl->build('comments/add_comment');	

}else{
	Notif::add('error','Paramètres incorrects');
}
