<?php
/**
* @author Darbon Bertrand
* @version 1.0
* @require $message de type Message contenant le message (article, discussion, page...) auquel est lié le commentaire
* @require $from de type string contenant l'url réécrite où se situe le formulaire (ex : article/a/4 )
* @return $comments -> html généré à placer dans la page avec tout les commentaires
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
$comments='';
if(!empty($message))
{
	$allComments = DBH::getList('Message', array('linkedTo' => $message->getId(), 'type' => TypesMessages::COMMENT));
	foreach ($allComments as $comment) {
		$tpl->value('author',$comment->getAuthorObj()->getPseudo());
		$tpl->value('userPicture',$comment->getAuthorObj()->getPicture());
		$tpl->value('signature',$Parsedown->text($comment->getAuthorObj()->getSignature()));
		if(!empty($user) &&  $comment->getAuthorObj()->getId()==$user->getId())
		{
			$tpl->value('editLink','<a href="'.HOST.'message/edit/'.$comment->getId().'">Modifier</a>');
		}else{
			$tpl->value('editLink','');
		}
		$tpl->value('text',$Parsedown->text($comment->getText()));
		$tpl->value('date',$comment->getDate());
		$tpl->value('id',$comment->getId());
		$comments.=$tpl->build('messages/message');
	}
}