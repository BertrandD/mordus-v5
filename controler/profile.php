<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
if(!empty($user))
{
	if(isset($_POST['save']))
	{
		if(!empty($_POST['picture']))
		{
			if($_POST['picture']!=$user->getPicture())
			{
				$user->setPicture($_POST['picture']);
				DBH::save($user);
				$tpl->value('picture',$user->getPicture());
				$success[]="Votre image de profile a bien été enregistré";
			}
		}
		if(!empty($_POST['signature']))
		{
			if($_POST['signature']!=$user->getSignature())
			{
				$user->setSignature($_POST['signature']);
				DBH::save($user);
				$tpl->value('signature',$user->getSignature());
				$success[]="Votre signature a bien été enregistrée";
			}
		}
	}

	if(!empty($success))
	{
		Notif::add('success',$success);
	}
	$tpl->value('signature',$user->getSignature());
	$tpl->value("formulaire", $tpl->build('user/form_edit_profile'));

	$page.=$tpl->build('user/profile');	
	$_links[]=array('txt' => 'profile');
}
