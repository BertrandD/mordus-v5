<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
$nb_articles = DBH::getCount('Message',array('type' => TypesMessages::ARTICLE));
$nb_per_page=10;
$first=0;

if(!empty($_GET['p']))
{
	$first=($_GET['p']-1)*$nb_per_page;
}
$tpl->getboucle('articles/boucle_pagination');
for ($k=1; $k <= ceil($nb_articles/$nb_per_page); $k++) { 
	if(!empty($_GET['p']) && $_GET['p']==$k || empty($_GET['p']) && $k==1)
	{
		$tpl->value('active','active');
		$tpl->value('num',$k);
	}else{
		$tpl->value('active','');
		$tpl->value('num',$k);
	}
	$tpl->boucle();
}
if(!empty($_GET['p']) && $_GET['p']==1 || empty($_GET['p'])){
	$tpl->value('disabledPrev','disabled');
	$tpl->value('prev','#');
}else{
	$tpl->value('prev',HOST.'accueil/p/'.($_GET['p']-1));
}
if(!empty($_GET['p']) && $_GET['p']==$k-1){
	$tpl->value('disabledNext','disabled');
	$tpl->value('next','#');
}else{
	if(!empty($_GET['p'])){
		$tpl->value('next',HOST.'accueil/p/'.($_GET['p']+1));
	}else{
		$tpl->value('next',HOST.'accueil/p/2');
	}
}

$tpl->value('pagination',$tpl->finboucle());
$articles = DBH::getList("Message", 
						array("type" => TypesMessages::ARTICLE), 
						array('date' => 'desc'), 
						$nb_per_page,
						$first);

$articlesTPL='';
foreach ($articles as $article) {
	$tpl->value('id',$article->getId());
	$tpl->value('image',$article->getImage());
	$tpl->value('titre',$article->getTitre());
	$tpl->value('titreUrl',$article->getTitreUrl());

	$nb = DBH::getCount('Message',array('type' => TypesMessages::COMMENT, 'linkedTo' => $article->getId()));
/*	if($nb>1)
		$nb.=' commentaires';
	else
		$nb.=' commentaire';*/
	$tpl->value('nb_comments',$nb);
	$tpl->value('author',$article->getAuthorObj()->getPseudo());
	$tpl->value('text',strip_tags(text_trunc($article->getText())));
	$tpl->value('date',$article->getDate());
	$articlesTPL.=$tpl->build('articles/article_thumb');
}

$tpl->value('articles',$articlesTPL);
$tpl->value('url','accueil');
$page.=$tpl->build('accueil');