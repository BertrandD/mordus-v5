<?php

if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($_GET['a']))
{
	$article = DBH::getUnique('Message', array('id' => $_GET['a'], 'type' => TypesMessages::ARTICLE));
	$tpl->value('id',$article->getId());
	$tpl->value('image',$article->getImage());
	$tpl->value('titre',$article->getTitre());
	$tpl->value('author',$article->getAuthorObj()->getPseudo());
	$tpl->value('signature',$Parsedown->text($article->getAuthorObj()->getSignature()));
	$tpl->value('userPicture',$article->getAuthorObj()->getPicture());
	if(!empty($user) && $user->getId() == $article->getAuthor())
	{
		$tpl->value('edit','(<a href="'.HOST.'add_article/a/'.$article->getId().'">editer</a>)');
	}else{
		$tpl->value('edit','');
	}
	$tpl->value('text',$article->getText());
	$tpl->value('date',$article->getDate());
	$tpl->value('message',$tpl->build('messages/message'));
	$message=$article;
	$from="article/a/".$_GET['a'];
	include('add_comment.php');
	include('comments.php');
	$tpl->value('comments',$comments);
	$tpl->value('add_comment',$add_comment);
	$page.=$tpl->build('articles/article');
	$_links[]=array('txt' => 'article');
	$_links[]=array('txt' => $article->getTitre());
}