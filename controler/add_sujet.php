<?php
/**
* @author Darbon Bertrand
* @version 1.0
* @require $message de type Message contenant le message (article, discussion, page...) auquel est lié le commentaire
* @require $from de type string contenant l'url réécrite où se situe le formulaire (ex : article/a/4 )
* @return $add_sujet -> html généré à placer dans la page avec le formulaire pour ajouter un commentaire
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

$add_sujet='';
if(empty($user))
{
	Notif::add('erreur','Vous devez être connecté pour ajouter un sujet');
}elseif(!empty($message) && !empty($from)){

	if(isset($_POST['add_sujet']))
	{
		extract($_POST);
		
		if(empty($textComment))
		{
			$erreurs[] = _("Merci d'écrire un sujet....");
		}
		
		if(empty($titre))
		{
			$erreurs[] = _("Merci de choisir un titre.");
		}

		if(empty($erreurs))
		{
			$a['id'] = DBH::getNextId('Message');
			$a['titre'] = $titre;
			$a['author'] = $user->getId();
			$a['type'] = TypesMessages::SUJET;
			$a['date'] = date("Y-m-d H:i:s");
			$a['linkedTo'] = $message->getId();
			$a['text'] = $Parsedown->text($textComment);
			$comment = DBH::create('Message',$a);
			DBH::save($comment);
		}

	}

	if(!empty($erreurs))
	{
			Notif::add('error',$erreurs);
	}

	$tpl->value('from',$from);

	if(empty($comment))
	{
		$tpl->value('formulaire',$tpl->build('comments/add_comment'));
	}else{
		header('Location: '.HOST.'forums/sujet/'.$comment->getId().'/'.nettoyerChaine($comment->getTitre()));
		Notif::add('success',_('Félicitation, le sujet a bien été enregistré !'));
	}

	$add_sujet.=$tpl->build('forums/add_sujet');	

}else{
	Notif::add('error','Paramètres incorrects');
}
