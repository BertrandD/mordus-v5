<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
if(!empty($user) && $user->getAccess_level() >= AccessLevel::PAGER)
{

	if($user->getAccess_level()==AccessLevel::ADMIN)
	{
		$tpl->value('title','Liste des pages');
		$_links[]=array('txt' => 'Liste des pages');
		$myPages = DBH::getList('Message', array('type' => TypesMessages::PAGE));
	}else{
		$tpl->value('title','Mes pages');
		$_links[]=array('txt' => 'Mes pages');
		$myPages = DBH::getList('Message', array('type' => TypesMessages::PAGE, 'author' => $user->getId()));
	}

	$tpl->getBoucle('pages/boucle_pages');
	foreach ($myPages as $myPage) {
		$tpl->value('id',$myPage->getId());
		$tpl->value('pTitre',$myPage->getTitre());
		$tpl->value('pAuthor',$myPage->getAuthorObj()->getPseudo());
		$tpl->value('pLink',HOST.'page/p/'.$myPage->getId());
		$tpl->value('pEdit',HOST.'add_page/edit/'.$myPage->getId());
		$tpl->boucle();
	}
	$tpl->value('pages',$tpl->finboucle());
	$page.=$tpl->build('pages/view_pages');

}