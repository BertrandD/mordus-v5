<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
if(!empty($user))
{
	$_REQUEST['fullalbum']=1; 
	ob_start();
	include 'foliogallery.php';
	$albums = ob_get_clean();
	$albums = str_replace('index.php?', 'view_files/', $albums);
	$albums = str_replace('album=', "album/", $albums);
	$albums = str_replace('HOST', HOST, $albums);
	$tpl->value('albums',$albums);
	$page.=$tpl->build('files/view_files');	
	$_links[]=array('txt' => 'médiathèque');
}
