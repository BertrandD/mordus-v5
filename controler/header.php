<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

$user = null;
$bagElemCount = 0;

if(isset($_SESSION['id']))
{
	$identifier=array(
		'id' => $_SESSION['id']
		);
	$user=DBH::getUnique('User',$identifier);
}

if(isset($_POST['login']) && isset($_POST['pass']))
{
	// Traitement des données
	$login = htmlspecialchars($_POST['login']);
	$pass = htmlspecialchars($_POST['pass']);

	// Le mot de passe arrive ici en étant cripté par javascript en sha1
	// Cette méhode de cryptage est un peu faible et peu adapté à des mots de passe
	// car plutôt pensée pour des fichiers, une utilisation de crypt() serait plus adaptée
	// mais n'étant pas comprise par mysql, il faudrait tester les mots de passe de la BDD
	// un par un avec cette condition :  hash_equals($hashed_password, crypt($user_input, $hashed_password))
	// où $hashed_password est le vrai mot de passe et $user_input le mot de passe entré par l'utilisateur
	// dans notre le cas, l'utilisation de sha1 est bien suffisante ! D'autant que nous
	// utilisons du HTTPS... 
	$identifier=array(
		"pseudo" => $login,
		"password" => $pass,
		);
	$user = DBH::getUnique('User',$identifier);

	if(empty($user))
	{
		$tpl->value('erreur', _("erreur :)"));
	}else{
		Notif::add('information',_('Salutation à vous '.$user->getPseudo().' !'));
		$_SESSION['id'] = $user->getId();
	}
}

if(empty($user))
{
	$tpl->value("user",$tpl->build('authentification/formulaire_connexion'));
}else{
	$tpl->value("userId", $user->getId());
	$tpl->value("pseudo", $user->getPseudo());
	$tpl->value("mail", $user->getMail());
	$tpl->value("picture", $user->getPicture());
	$buttons="";

	$admin='';
	$tpl->value('AccessLevel',AccessLevel::toString($user->getAccess_level()));

	if($user->getAccess_level()>AccessLevel::USER){
		$admin .= $tpl->build('user/access/common');
	}

	if($user->getAccess_level()==AccessLevel::ADMIN)
	{
		$admin .= $tpl->build('user/access/admin');
	}
	if($user->getAccess_level()>=AccessLevel::PAGER)
	{
		$admin.= $tpl->build('user/access/pager');
	}
	if($user->getAccess_level()>=AccessLevel::AUTHOR)
	{
		$admin.= $tpl->build('user/access/author');
	}

	$tpl->value('admin',$admin);
	$tpl->value("user",$tpl->build('user/infos_user'));
}


$lastTopics = DBH::getList('Message',array('type' => TypesMessages::SUJET),array('date' => 'desc'),5);

$tpl->getBoucle('forums/lastTopics');
foreach ($lastTopics as $topic) {
	$tpl->value('topicLink',HOST.'forums/sujet/'.$topic->getId().'/'.nettoyerChaine($topic->getTitre()));
	$tpl->value('topicTitre',$topic->getTitre());
	$tpl->value('topicAuthor',$topic->getAuthorObj()->getPseudo());
	$tpl->value('topicDate',getDateInterval($topic->getDate()));
	$tpl->boucle();
}
$tpl->value('lastTopics',$tpl->finboucle());


$lastComments = DBH::getList('Message',array('type' => TypesMessages::COMMENT),array('date' => 'desc'),5);

$tpl->getBoucle('forums/lastComments');
foreach ($lastComments as $comment) {
	$tpl->value('topicLink',$comment->getLink());
	$tpl->value('topicTitre',$comment->getLinkedObj()->getTitre());
	$tpl->value('topicAuthor',$comment->getAuthorObj()->getPseudo());
	$tpl->value('topicDate',getDateInterval($comment->getDate()));
	$tpl->boucle();
}
$tpl->value('lastComments',$tpl->finboucle());