<?php
/**
* @author Darbon Bertrand
* @version 1.0
*/
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($user) && $user->getAccess_level() >= AccessLevel::PAGER)
{

	if(isset($_POST['add_page']))
	{
		extract($_POST);
		
		if(empty($titre))
		{
			// pas de check sur la longueur du password du fait qu'il
			// sera hash avant d'arriver ici (javascript) il aura donc
			// forcément une certaine longueur.
			$erreurs[] = _("Merci d'indiquer un titre");
		}
		
		if(empty($text))
		{
			// pas de check sur la longueur du password du fait qu'il
			// sera hash avant d'arriver ici (javascript) il aura donc
			// forcément une certaine longueur.
			$erreurs[] = _("Merci d'écrire un contenu....");
		}

		if(empty($erreurs))
		{
			if(!empty($_GET['edit']))
			{
				$_POST['id'] = $_GET['edit'];
			}
			$_POST['author'] = $user->getId();
			$_POST['type'] = TypesMessages::PAGE;
			$_POST['date'] = date("Y-m-d H:i:s");
			$Page = DBH::create('Message',$_POST);
			DBH::save($Page);
			$Page = DBH::getUnique('Message',array('date' => $_POST['date'], 'author' => $user->getId(), 'type' => TypesMessages::PAGE));
		}else{
			foreach ($_POST as $key => $value) {
				$tpl->value($key,$value);
			}
		}

	}

	if(isset($_GET['edit']))
	{
		$pageEdit = DBH::getUnique('Message',array('type' => TypesMessages::PAGE, 'author' => $user->getId(), 'id' => $_GET['edit']));
		if(!empty($pageEdit))
		{
			$tpl->value('titre',$pageEdit->getTitre());
			$tpl->value('imageLeft',$pageEdit->getImageLeft());
			$tpl->value('imageRight',$pageEdit->getImageRight());
			$tpl->value('text',$pageEdit->getText());
			$tpl->value('edit','/edit/'.$_GET['edit']);
		}else{
			$erreurs[]="Impossible d'éditer cette page";
		}
	}

	if(!empty($erreurs))
	{
		Notif::add('error',$erreurs);
	}

	if(empty($Page))
	{
		$tpl->value('formulaire',$tpl->build('pages/form_add_page'));
	}else{
		$tpl->value('formulaire','<a href="'.HOST.'page/p/'.$Page->getId().'">'.$Page->getTitre().'</>');
		Notif::add('success',_('Félicitation, la page a bien été enregistrée !'));
	}

	$page.=$tpl->build('pages/add_page');	
	$_links[]=array('txt' => 'ajouter une page');
}
