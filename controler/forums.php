<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
$_links[]=array('txt' => 'forums', 'link' => 'forums');

if(!empty($_GET['sujet']))
{
	$subject = DBH::getUnique('Message',array('type' => TypesMessages::SUJET, 'id' => $_GET['sujet']));
	if(!empty($subject)){
		$tpl->value('id',$subject->getId());
		$tpl->value('titre',$subject->getTitre());
		$tpl->value('author',$subject->getAuthorObj()->getPseudo());
		$tpl->value('signature',$Parsedown->text($subject->getAuthorObj()->getSignature()));
		$tpl->value('userPicture',$subject->getAuthorObj()->getPicture());
		if(!empty($user) && $user->getId() == $subject->getAuthor())
		{
			$tpl->value('editLink','<a href="'.HOST.'message/edit/'.$subject->getId().'">Modifier</a>');
		}else{
			$tpl->value('editLink','');
		}
		$tpl->value('text',$Parsedown->text($subject->getText()));
		$tpl->value('date',$subject->getDate());
		$tpl->value('sujet',$tpl->build('messages/message'));
		$message=$subject;
		$from="forums/sujet/".$_GET['sujet'];
		include('add_comment.php');
		include('comments.php');
		$tpl->value('comments',$comments);
		$tpl->value('add_comment',$add_comment);
		$page.=$tpl->build('forums/sujet');
		$forum=DBH::getUnique('Forum',array('id' => $subject->getLinkedTo()));
		$category=DBH::getUnique('Category',array('id' => $forum->getCategory()));
		$_links[]=array('txt' => $category->getNom(), 'link' => 'forums/category/'.$category->getId().'/'.nettoyerChaine($category->getNom()));
		$_links[]=array('txt' => $forum->getNom(), 'link' => 'forums/forum/'.$forum->getId().'/'.nettoyerChaine($forum->getNom()));
		$_links[]=array('txt' => $subject->getTitre());
	}else{
		Notif::add('error','Le sujet n\'existe pas ou plus');
	}


}elseif (!empty($_GET['forum'])) {
	$forum = DBH::getUnique('Forum',array('id' => $_GET['forum']));
	if(!empty($forum)){

		$message=$forum;
		$from="forums/forum/".$forum->getId();
		$type='sujet';
		include('add_sujet.php');
		$tpl->value('newTopic',$add_sujet);

		$sujets = DBH::getList('Message',array('type' => TypesMessages::SUJET, 'linkedTo' => $forum->getId()));
		$sujetTpl = '';
		$tpl->getBoucle('forums/category');
		foreach ($sujets as $sujet) {
			$lastComment = DBH::getUnique('Message',array('linkedTo' => $sujet->getId(), 'type' => TypesMessages::COMMENT), array('date' => 'desc'));
			if(!empty($lastComment)){
				$tpl->value('lastTopicLink',HOST.'forums/sujet/'.$sujet->getId().'/'.nettoyerChaine($sujet->getTitre()).'#'.$lastComment->getId());

				$tpl->value('lastTopicTitre','Dernière réponse');
				$tpl->value('lastTopicAuthor',$lastComment->getAuthorObj()->getPseudo());
				$tpl->value('lastTopicDiffDate',getDateInterval($lastComment->getDate()));
			}else{
				$tpl->value('lastTopicTitre','');
			}
			$nbComments = DBH::getcount('Message',array('linkedTo' => $sujet->getId(), 'type' => TypesMessages::COMMENT));
			$tpl->value('nbSubject',$nbComments);
			if($nbComments<=1){
				$tpl->value('sujectsType','réponse');
			}else{
				$tpl->value('sujectsType','réponses');
			}
			$tpl->value('topicLink',HOST.'forums/sujet/'.$sujet->getId().'/'.nettoyerChaine($sujet->getTitre()));
			$tpl->value('topicTitre',$sujet->getTitre());
			$tpl->value('topicDesc','Par '.$sujet->getAuthorObj()->getPseudo().' le '.$sujet->getDate().'');
			$tpl->boucle();
		}
		$sujetTpl.=$tpl->finboucle();
		$tpl->value('categories',$sujetTpl);
		$tpl->value('titre',$forum->getNom());

		$page.=$tpl->build('forums/forums');
		$category=DBH::getUnique('Category',array('id' => $forum->getCategory()));
		$_links[]=array('txt' => $category->getNom(), 'link' => 'forums/category/'.$category->getId().'/'.nettoyerChaine($category->getNom()));
		$_links[]=array('txt' => $forum->getNom());
	}else{
		Notif::add('error','Le forum n\'existe pas ou plus');
	}
}elseif (!empty($_GET['category'])) {
		$category = DBH::getUnique('Category',array('id' => $_GET['category']));
		if(!empty($category)){
			$forums = DBH::getList('Forum',array('category' => $category->getId()));
			$tpl->getBoucle('forums/category');
			$categoriesTpl = '';
			foreach ($forums as $forum) {
				$lastTopic = DBH::getUnique('Message',array('linkedTo' => $forum->getId(), 'type' => TypesMessages::SUJET), array('date' => 'desc'));
				if(!empty($lastTopic)){
					$tpl->value('lastTopicLink',HOST.'forums/sujet/'.$lastTopic->getId().'/'.nettoyerChaine($lastTopic->getTitre()));
					$tpl->value('lastTopicTitre',$lastTopic->getTitre());
					$tpl->value('lastTopicAuthor',$lastTopic->getAuthorObj()->getPseudo());
					$tpl->value('lastTopicDiffDate',getDateInterval($lastTopic->getDate()));
				}
				$nbSubject = DBH::getcount('Message',array('linkedTo' => $forum->getId(), 'type' => TypesMessages::SUJET));
				$tpl->value('nbSubject',$nbSubject);
				if($nbSubject<=1){
					$tpl->value('sujectsType','sujet');
				}else{
					$tpl->value('sujectsType','sujets');
				}
				$tpl->value('topicLink',HOST.'forums/forum/'.$forum->getId().'/'.nettoyerChaine($forum->getNom()));
				$tpl->value('topicTitre',$forum->getNom());
				$tpl->value('topicDesc',$forum->getDescription());
				$tpl->boucle();
			}
			$categoriesTpl.=$tpl->finboucle();

			$tpl->value('categories',$categoriesTpl);
			$tpl->value('titre',$category->getNom());

			$page.=$tpl->build('forums/forums');
			$_links[]=array('txt' => $category->getNom(), 'link' => 'forums/category/'.$category->getId().'/'.nettoyerChaine($category->getNom()));
		}else{
			Notif::add('error','La catégorie n\'existe pas ou plus');
		}
		
}else{
	$categories = DBH::getList('Category',array(), array('priority' => 'asc'));
	$categoriesTpl = '';
	foreach ($categories as $category) {
		$forums = DBH::getList('Forum',array('category' => $category->getId()));
		$tpl->value('cateNom',$category->getNom());
		$tpl->getBoucle('forums/category');
		foreach ($forums as $forum) {
			$lastTopic = DBH::getUnique('Message',array('linkedTo' => $forum->getId(), 'type' => TypesMessages::SUJET), array('date' => 'desc'));
			if(!empty($lastTopic)){
				$tpl->value('lastTopicLink',HOST.'forums/sujet/'.$lastTopic->getId().'/'.nettoyerChaine($lastTopic->getTitre()));
				$tpl->value('lastTopicTitre',$lastTopic->getTitre());
				$tpl->value('lastTopicAuthor',$lastTopic->getAuthorObj()->getPseudo());
				$tpl->value('lastTopicDiffDate',getDateInterval($lastTopic->getDate()));
			}else{
				$tpl->value('lastTopicTitre','');
			}
			$nbSubject = DBH::getcount('Message',array('linkedTo' => $forum->getId(), 'type' => TypesMessages::SUJET));
			$tpl->value('nbSubject',$nbSubject);
			if($nbSubject<=1){
				$tpl->value('sujectsType','sujet');
			}else{
				$tpl->value('sujectsType','sujets');
			}
			$tpl->value('topicLink',HOST.'forums/forum/'.$forum->getId().'/'.nettoyerChaine($forum->getNom()));
			$tpl->value('topicTitre',$forum->getNom());
			$tpl->value('topicDesc',$forum->getDescription());
			$tpl->boucle();
		}
		$categoriesTpl.=$tpl->finboucle();
	}

	$tpl->value('categories',$categoriesTpl);
	$tpl->value('titre','Liste des forums');

	$page.=$tpl->build('forums/forums');
}

