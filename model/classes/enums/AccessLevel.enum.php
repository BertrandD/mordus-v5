<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @author Leboc Philippe
* @version 1.0
* @Descr = Définit les droits d'accès
*/
abstract class AccessLevel{
	const USER = 0;
	const AUTHOR = 1;
	const PAGER = 2;
	const MODERATOR = 50;
	const ADMIN = 100;

	public static function toString($value){
		$result = NULL;

		switch($value)
		{
			case self::USER:
				$result = "Membre";
				break;
			case self::AUTHOR:
				$result = "Auteur";
				break;
			case self::PAGER:
				$result = "Auteur";
				break;
			case self::MODERATOR:
				$result = "Modérateur";
				break;
			case self::ADMIN:
				$result = "Administrateur";
				break;
			default:
				break;
		}

		return $result;
	}
}