<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @author Darbon Bertrand
* @version 1.0
* @Descr = Définit les droits d'accès
*/
abstract class TypesMessages{
	const ARTICLE = 1;
	const SUJET = 2;
	const COMMENT = 4;
	const PAGE = 5;

	public static function toString($value){
		$result = NULL;

		switch($value)
		{
			case 0:
				$result = "Commentaire";
				break;
			case 1:
				$result = "Article";
				break;
			case 2:
				$result = "Discussion";
				break;
			case 3:
				$result = "Réponse";
				break;
			default:
				break;
		}

		return $result;
	}
}