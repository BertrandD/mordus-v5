<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

class Category extends Object
{
	static $table = "category";

	protected $id;
	protected $nom;
	protected $priority;

	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}

    public function getIdentity()
    {
      return array(
          "id" =>$this->getId()
        );
    }

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNom(){
		return $this->nom;
	}

	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getPriority(){
		return $this->priority;
	}

	public function setPriority($priority){
		$this->priority = $priority;
	}

}