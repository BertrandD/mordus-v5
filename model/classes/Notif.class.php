<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
/**
 * @name = Notif
 * @author = Bertrand Darbon
 * @descr = Gestion des notifications
 * @require = class templates
 */
abstract class Notif {

	private static $_notifs = null;

	/**
	* @name : add()
	* @Descr : Ajouter une notification au système
	*/
	static public function add($type, $notif){
		if(is_array($notif))
		{
			foreach ($notif as $n) {
				self::$_notifs[] = array("type" => $type, "message" => $n);
			}
		}else{
			self::$_notifs[] = array("type" => $type, "message" => $notif);
		}
	}

	/**
	* @return : le code html à afficher
	* @name : display()
	* @Descr : Afficher toute les notifications
	* @Parameters : $tpl : objet facultatif de type templates
	*/
	static public function display(templates $tpl){
		$page="";
		if(!empty(self::$_notifs))
		{
			foreach (self::$_notifs as $notif) {
				$tpl->value('message',addslashes($notif['message']));
				$tpl->value('type',$notif['type']);
				$tpl->value('layout','bottomRight');
				$page.=$tpl->build('notif/notif');
			}
		}
		return $page;
	}
}