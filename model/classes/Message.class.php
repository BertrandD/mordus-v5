<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

class Message extends Object // ce nom est réservé par php, n'essayez surtout pas d'inclure ce fichier qq part :P
{
	static $table = "messages";

	protected $id;
	protected $type;
	protected $image;
	protected $titre;
	protected $text;
	protected $date;
	protected $author;
	protected $linkedTo;
	protected $imageLeft;
	protected $imageRight;	
	private $authorObj;
	private $linkedObj;

	/**
	*	Constructeur, rarement besoin de le modifié
	* Pour créer un objet on passe les valeurs des attributs en paramètres
	* sous forme d'un tableau
	*/
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}

    public function getIdentity()
    {
      return array(
          "id" =>$this->getId()
        );
    }

    public function getAuthorObj()
    {
    	if(empty($this->authorObj))
    	{
    		$this->authorObj = DBH::getUnique('User',array('id' => $this->author));
    	}
    	return $this->authorObj;
    }

	/**
	*	@Name : isValide()
	*	@Descr : Nécessaire pour l'enregistrement en base de donnée, permet la vérification par le DAO
	*			qu'un objet dispose bien des attributs indispensable (clé primaire).
	*/
	public function isValide(){
		if(!empty($this->text))
			return true;
		else
			return false;
	}	

	public function getTitreUrl()
	{
		$url = str_replace("-", "", $this->getTitre());
		$url = str_replace("  ", "-", $this->getTitre());
		$url = str_replace(" ", "-", $this->getTitre());
		return $url;
	}

	public function getLink()
	{
		switch ($this->type) {
			case TypesMessages::ARTICLE:
					return HOST.'article/a/'.$this->id;
				break;
			case TypesMessages::SUJET:
					return HOST.'forums/sujet/'.$this->id.'/'.$this->getTitreUrl();
				break;
			case TypesMessages::COMMENT:
					$parent = DBH::getUnique('Message',array('id' => $this->getLinkedTo()));
					return $parent->getLink().'#'.$this->id;
				break;
		}
	}

	public function getLinkedObj()
	{
    	if(empty($this->linkedObj))
    	{
			switch ($this->type) {
				case TypesMessages::ARTICLE:
						return false;
					break;
				case TypesMessages::SUJET:
						$type="forum";
					break;
				case TypesMessages::COMMENT:
						$type="message";
					break;
			}
    		$this->linkedObj = DBH::getUnique($type,array('id' => $this->linkedTo));
    	}
    	return $this->linkedObj;
	}

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getType(){
		return $this->type;
	}

	public function setType($type){
		$this->type = $type;
	}
	
	public function getImage(){
		return $this->image;
	}

	public function setImage($image){
		$this->image = $image;
	}

	public function getTitre(){
		return $this->titre;
	}

	public function setTitre($titre){
		$this->titre = $titre;
	}

	public function getText(){
		return $this->text;
	}

	public function setText($text){
		$this->text = $text;
	}

	public function getDate(){
		return $this->date;
	}

	public function setDate($date){
		$this->date = $date;
	}

	public function getAuthor(){
		return $this->author;
	}

	public function setAuthor($author){
		$this->author = $author;
	}
	
	public function getLinkedTo(){
		return $this->linkedTo;
	}

	public function setLinkedTo($linkedTo){
		$this->linkedTo = $linkedTo;
	}

	public function getImageLeft(){
		return $this->imageLeft;
	}

	public function setImageLeft($imageLeft){
		$this->imageLeft = $imageLeft;
	}

	public function getImageRight(){
		return $this->imageRight;
	}

	public function setImageRight($imageRight){
		$this->imageRight = $imageRight;
	}
}
