<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

class Upload extends Object // ce nom est réservé par php, n'essayez surtout pas d'inclure ce fichier qq part :P
{
	static $table = "uploads";

	protected $id;
	protected $user;
	protected $file;
	protected $folder;
	protected $date;
	private $userObj;

	/**
	*	Constructeur, rarement besoin de le modifié
	* Pour créer un objet on passe les valeurs des attributs en paramètres
	* sous forme d'un tableau
	*/
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}

    public function getIdentity()
    {
      return array(
          "id" =>$this->getId()
        );
    }

    public function getUserObj()
    {
    	if(empty($this->userObj))
    	{
    		$this->userObj = DBH::getUnique('User',array('id' => $this->user));
    	}
    	return $this->userObj;
    }

    public function getThumb()
    {
    	return 'files/'.$this->getFolder().'/thumbs/'.$this->getFile();
    }

    public function getPath()
    {
    	return 'files/'.$this->getFolder().'/'.$this->getFile();
    }

	/**
	*	@Name : isValide()
	*	@Descr : Nécessaire pour l'enregistrement en base de donnée, permet la vérification par le DAO
	*			qu'un objet dispose bien des attributs indispensable (clé primaire).
	*/
	public function isValide(){
		if(!empty($this->file))
			return true;
		else
			return false;
	}	

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getUser(){
		return $this->user;
	}

	public function setUser($user){
		$this->user = $user;
	}

	public function getFolder(){
		return $this->folder;
	}

	public function setFolder($folder){
		$this->folder = $folder;
	}

	public function getFile(){
		return $this->file;
	}

	public function setFile($file){
		$this->file = $file;
	}

	public function getDate(){
		return $this->date;
	}

	public function setDate($date){
		$this->date = $date;
	}
}
