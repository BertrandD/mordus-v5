<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
 
abstract class Object{
	
	public function __construct($data = array()){
      if(!empty($data)){
        $this->hydrate($data);
      }else{
        echo "$data vide.";
      }
    }
    
    public function hydrate($data){
      foreach($data as $key=>$value){
        $methode = 'set'.ucfirst($key);
        //echo $methode."\n";
        if(is_callable(array($this, $methode))){
          //echo $methode."(".$value.")\n";
          $this->$methode($value);
        }else{
          // TODO: DEBUG
          // Une erreur dans le code est révélée ici vis-à-vis des informations de l'utilisateur contenues dans $_POST (immortel ?)
          // De plus, l'affichage de l'echo cause un trouble d'affichage
          //echo "Problème d'hydratation\n";
        }
      }
    }

    /*
    * @Name : isValide()
    * @Descr : Nécessaire pour l'enregistrement en base de donnée, permet la vérification par le DAO
    *     qu'un objet dispose bien des attributs indispensable (equiv clé primaire).
    */
    public function isValide()
    {
      $array = $this->getIdentity();
      foreach ($array as $key => $value) {
        if(empty($value)) 
          return false;
      }
      return true;
    }

	/*
	* @Name : getIdentity()
	* @Descr : Définit ce qui fait de l'objet son identité,
	*			ie : ce qui l'identifie aux autres / sa clé primaire dans la BDD
	* @Return : Un tableau clé => valeur
	Exemple :
					public function getIdentity()
					{
						return array(
								"id" =>$this->id
							);			
					}
	*/
    public abstract function getIdentity();

	/* 
	 * @Name : iterate()
	 * @Descr : Renvoie un tableau avec toutes les valeurs de l'objet
	 */
	public function iterate(){
		return get_object_vars($this);
	}


}
