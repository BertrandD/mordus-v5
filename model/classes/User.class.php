<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = User
* @author = Bertrand Darbon
* @Descr = Sert d'objet User.
*/
class User extends Object {
	
	public static $table = 'user';

	protected $id; 			//int
	protected $pseudo;		//string
	protected $mail;		//string
	protected $access_level;//int
	protected $password;	//string
	protected $picture;	//string
	protected $signature;	//string
	protected $confirmPassword;	//string
	
	// Constructeur
    public function __construct($data = array()){
        parent::__construct($data);
    }

    public function getIdentity()
    {
      return array(
          "mail" =>$this->getMail()
        );
    }

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getPseudo(){
		return $this->pseudo;
	}

	public function setPseudo($pseudo){
		$this->pseudo = $pseudo;
	}

	public function getMail(){
		return $this->mail;
	}

	public function setMail($mail){
		$this->mail = $mail;
	}

	public function getAccess_level(){
		return $this->access_level;
	}

	public function setAccess_level($access_level){
		$this->access_level = $access_level;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getPicture(){
		if(empty($this->picture))
			return HOST.'files/gallery/inconnu.png';
		else
			return $this->picture;
	}

	public function setPicture($picture){
		$this->picture = $picture;
	}
	
	public function getSignature(){
		return $this->signature;
	}

	public function setSignature($signature){
		$this->signature = $signature;
	}
		
	public function getConfirmPassword(){
		return $this->confirmPassword;
	}

	public function setConfirmPassword($confirmPassword){
		$this->confirmPassword = $confirmPassword;
	}
}
?>
