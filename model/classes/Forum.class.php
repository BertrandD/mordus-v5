<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

class Forum extends Object
{
	static $table = "forums";

	protected $id;
	protected $nom;
	protected $description;
	protected $category;

	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}

    public function getIdentity()
    {
      return array(
          "id" =>$this->getId()
        );
    }

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNom(){
		return $this->nom;
	}

	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getCategory(){
		return $this->category;
	}

	public function setCategory($category){
		$this->category = $category;
	}	

}