<?php

function printR($var)
{
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}

function getDateInterval($date)
{
	$date1 = new DateTime(date("Y-m-d H:i:s"));
	$date2 = new DateTime($date);
	$interval = $date1->diff($date2);
	$intervalTpl ='';
	if($interval->y!=0){
		$intervalTpl.=$interval->y.' année ';
	}
	if($interval->m!=0){
		$intervalTpl.=$interval->m.' mois ';
	}else{ // si plusieurs mois on fait pas le reste
		if($interval->d!=0){
			$intervalTpl.=$interval->d.' jours ';
		}else{ // si plusieurs jours on fait pas le reste
			if($interval->h!=0){
				$intervalTpl.=$interval->h.' heures ';
			}
			if($interval->i!=0){
				$intervalTpl.=$interval->i.' minutes ';
			}else{
				if($interval->s!=0){
					$intervalTpl.=$interval->s.' secondes ';
				}		
			}	
		}		
	}
	return $intervalTpl;
}

function randomString($car) {
$string = "";
$chaine = "abcdefghijklmnpqrstuvwxyz0123456789";
srand((double)microtime()*1000000);
for($i=0; $i<$car; $i++) {
$string .= $chaine[rand()%strlen($chaine)];
}
return $string;
}

function make_thumb($folder,$file,$dest,$thumb_width) {

	$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
	
	switch($ext)
	{
		case "jpg":
		$source_image = imagecreatefromjpeg($folder.'/'.$file);
		break;
		
		case "jpeg":
		$source_image = imagecreatefromjpeg($folder.'/'.$file);
		break;
		
		case "png":
		$source_image = imagecreatefrompng($folder.'/'.$file);
		break;
		
		case "gif":
		$source_image = imagecreatefromgif($folder.'/'.$file);
		break;
	}	
	
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	if($width < $thumb_width) // if original image is smaller don't resize it
	{
		$thumb_width = $width;
		$thumb_height = $height;
	}
	else
	{
		$thumb_height = floor($height*($thumb_width/$width));
	}
	
	$virtual_image = imagecreatetruecolor($thumb_width,$thumb_height);
	
	if($ext == "gif" or $ext == "png") // preserve transparency
	{
		imagecolortransparent($virtual_image, imagecolorallocatealpha($virtual_image, 0, 0, 0, 127));
		imagealphablending($virtual_image, false);
		imagesavealpha($virtual_image, true);
    }
	
	imagecopyresampled($virtual_image,$source_image,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
	var_dump($virtual_image);
	switch($ext)
	{
	    case 'jpg': imagejpeg($virtual_image, $dest,80); break;
		case 'jpeg': imagejpeg($virtual_image, $dest,80); break;
		case 'gif': imagegif($virtual_image, $dest); break;
		case 'png': imagepng($virtual_image, $dest); break;
    }

	imagedestroy($virtual_image); 
	imagedestroy($source_image);
	
}

function breadcrumb($links=array())
{
	$s = '<ol class="breadcrumb">';
	$nb = count($links);
	$k=1;
	foreach ($links as $link) {
		if($k==$nb){
			$s .='<li class="active">'.ucfirst($link['txt']).'</li>';
		}else{
			if(!empty($link['link'])){
				$s .='<li><a href="'.HOST.$link['link'].'">'.ucfirst($link['txt']).'</a></li>';
			}else{
				$s .='<li class="active">'.ucfirst($link['txt']).'</li>';
			}

		}
		$k++;
	}
	$s.='</ol>';
	return $s;
}

/**
* text_trunc
* Coupe une chaine sans couper les mots
*
* @param string $texte Texte à couper
* @param integer $nbreCar Longueur à garder en nbre de caractères
* @return string
*/
function text_trunc($texte, $nbchar=500)
{
    return (strlen($texte) > $nbchar ? substr(substr($texte,0,$nbchar),0,strrpos(substr($texte,0,$nbchar)," "))." (...)" : $texte);
}


function nettoyerChaine($chaine)
{
	$caracteres = array(
		'À' => 'a', 'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
		'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', '€' => 'e',
		'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
		'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
		'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'µ' => 'u',
		'Œ' => 'oe', 'œ' => 'oe',
		'$' => 's');
 
	$chaine = strtr($chaine, $caracteres);
	$chaine = preg_replace('#[^A-Za-z0-9\.]+#', '-', $chaine);
	$chaine = trim($chaine);
	$chaine = strtolower($chaine);
 
	return $chaine;
}

/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité des textes provenant essentiellement des champs (ex: input type="text")
*		Une limitation HTML est imposée mais ce check est également obligatoire et primordial !
* @condition !empty($text) && ($min <= $text <= $max) && (is_string($text))
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_string($text, $min = 2, $max = 16){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$text = htmlspecialchars($text);

	// début des check
	if(!empty($text)){
		if(is_string($text) && strlen($text) >= $min && strlen($text) <= $max){
			$res = true;
		}
	}

	return $res;
}

/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité des nombres provenant essentiellement des champs (ex: input type="number")
*		Une limitation HTML est imposée mais ce check est également obligatoire et primordial !
* @condition !empty($number) && (is_numeric($number)) && ($min <= $number <= max)
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_numeric($number, $min = 0, $max = 100){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$number = htmlspecialchars($number);

	// début des check
	// ATTENTION : $var = 0 est considérée comme EMPTY !!!
	if(isset($number)){
		if((is_numeric($number)) && ($number >= $min) && ($number <= $max)){
			$res = true;
		}
	}

	return $res;
}

/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité du champ provenant essentiellement des champs (ex: input type="text")
*		Aucune limitation par le navigateur pour les numéro de téléphone.
* @condition !empty($phone) && $phone est au bon format
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_phone($phone){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$phone = htmlspecialchars($phone);

	// début des check
	if(!empty($phone)){
		if(preg_match('/^[+]?([\d]{0,3})?[\(\.\-\s]?([\d]{3})[\)\.\-\s]*([\d]{3})[\.\-\s]?([\d]{4})$/', $phone)){
			$res = true;
		}
	}

	return $res;
}


/**
* @author Leboc Philippe
* @version 1.0
* @descr Test la conformité de l'adresse e-mail provenant essentiellement des champs (ex: input type="email")
*		Certains navigateurs ne check pas le type email mais dans tous les cas cette vérification est obligatoire.
* @condition !empty($email) && (is_mail($number)) && (7 <= $email <= 30)
* @return True si le paramètre est jugé conforme aux normes 
*		établies par nos soins concernant le format de données 
*		reçu par un utilisateur. False sinon.
*/
function validate_mail($email){
	$res = false;

	// Transforme les caractères htmls en caractères courant
	$email = htmlspecialchars($email);

	// début des check
	if(!empty($email)){
		if(filter_var($email, FILTER_VALIDATE_EMAIL) && (strlen($email) >= 7) && (strlen($email) <= 50)){
			$res = true;
		}
	}

	return $res;
}

/**
* @author Leboc Philippe
* @version 1.0
*/
function validate_file($file, $userId){

	// Utile à savoir :
	//$_FILES['fichier']['name']	 //Le nom original du fichier, comme sur le disque du visiteur (exemple : mon_icone.png).
	//$_FILES['fichier']['type']     //Le type du fichier. Par exemple, cela peut être « image/png ».
	//$_FILES['fichier']['size']     //La taille du fichier en octets.
	//$_FILES['fichier']['tmp_name'] //L'adresse vers le fichier uploadé dans le répertoire temporaire.
	//$_FILES['fichier']['error']    //Le code d'erreur, qui permet de savoir si le fichier a bien été uploadé.

	$continue = TRUE;
	$erreur = NULL;
	$result = NULL;

	// Fixe la taille max en octets
	$maxsize = 51200;
	$maxwidth = 600;
	$maxheight = 600;

	// Fixe les extensions valides
	$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );

	// Recupère l'extension
	$extension_upload = strtolower(substr(strrchr($_FILES['fichier']['name'], '.'), 1));

	if($_FILES['fichier']['error'] > 0){
		// Une erreur est servenue, on s'arrête
		$continue = FALSE;
	}else{
		// Traitement de la taille du fichier (en octets)
		if($continue && ($_FILES['fichier']['size'] > $maxsize)){
			$erreur = "Le fichier est trop gros";
			$continue = FALSE;
		}

		// Traitement de l'extension
		if ($continue && (!in_array($extension_upload,$extensions_valides))){
			$erreur = "Extension incorrecte";
			$continue = FALSE;
		}

		// traitement des dimensions de l'image
		$image_sizes = getimagesize($_FILES['fichier']['tmp_name']);

		if($continue && ($image_sizes[0] > $maxwidth || $image_sizes[1] > $maxheight)){
			$erreur = "Image trop grande";
			$continue = FALSE;
		}

		if($continue){
			$nom = UPLOAD_DIR. $userId . "_" . $_FILES['fichier']['name'];
			$transfert = move_uploaded_file($_FILES['fichier']['tmp_name'], $nom);
			
			if (!$transfert){
				$continue = FALSE;
			}
		}
	}

	if($continue){
		$result = $nom;
	}

	return $result;
}