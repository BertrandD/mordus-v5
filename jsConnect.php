<?php
header('Content-Type: text/html; charset=utf-8');
session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL | E_STRICT);

define('PROJECT_DIR', realpath('./'));
if($_SERVER['SERVER_NAME']=='localhost')
{
	define('HOST','http://localhost:8080/mordus/');
}else{
	define('HOST','http://v5.lesmordusduclavier.fr/');
}
define("FRONT_CONTROLER", "Yes I'm coming from front controler !");
define("MODEL_DIR", PROJECT_DIR."/model/");
define("CLASS_DIR", PROJECT_DIR."/model/classes/");
define("ENUM_DIR", PROJECT_DIR."/model/classes/enums/");
define("EXCEPTION_DIR", PROJECT_DIR."/model/classes/exception/");
define("CONTROLER_DIR", PROJECT_DIR."/controler/");
define("TPL_DIR", PROJECT_DIR."/templates/");
define("UPLOAD_DIR", PROJECT_DIR."/ressources/images/uploads/");
define("LIB_DIR", PROJECT_DIR."/lib/");

define('LOCALE_DIR', PROJECT_DIR.'/locale');
define('DEFAULT_LOCALE', 'fr_FR');

require_once EXCEPTION_DIR.'FrontControlerException.class.php';
require_once EXCEPTION_DIR.'NotSupportedMethodException.class.php';

require_once ENUM_DIR.'AccessLevel.enum.php';
require_once ENUM_DIR.'TypesMessages.enum.php';

require_once CLASS_DIR.'Database.class.php';
require_once CLASS_DIR.'DBH.class.php';
require_once CLASS_DIR.'Object.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'Message.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'Templates.class.php';

require_once MODEL_DIR.'functions.php';

require_once LIB_DIR.'gettext/gettext.inc';

$supported_locales = array('fr_FR');
$encoding = 'UTF-8';

if(isset($_GET['lang']))
{
	$locale = $_GET['lang'];
	setcookie("lang",$locale);
}else if(isset($_COOKIE['lang'])){
	$locale = $_COOKIE['lang'];
}else{
	$locale = DEFAULT_LOCALE;
}
//$locale = (isset($_GET['lang']))? $_GET['lang'] : DEFAULT_LOCALE;

// gettext setup
T_setlocale(LC_MESSAGES, $locale.'.utf8');
// Set the text domain as 'messages'
$domain = 'default';
bindtextdomain($domain, LOCALE_DIR);
// bind_textdomain_codeset is supported only in PHP 4.2.0+
if (function_exists('bind_textdomain_codeset')) 
  bind_textdomain_codeset($domain, $encoding);
textdomain($domain);
$tpl = new Templates;
$tpl->DEBUG = false; // Pour afficher les {{message}} qui ne sont pas remplacés par le code php, changer en true
include(CONTROLER_DIR."header.php");

require_once dirname(__FILE__).'/functions.jsconnect.php';

// 1. Get your client ID and secret here. These must match those in your jsConnect settings.
$clientID = '1306705574';
$secret = '96462e83830f9b662295ecbea5d85a93';
// 2. Grab the current user from your session management system or database here.
if(!empty($user))
{
	$signedIn=true;
}else{
	$signedIn=false;
}
// 3. Fill in the user information in a way that Vanilla can understand.
$userJs = array();
if ($signedIn) {
   // CHANGE THESE FOUR LINES.
   $userJs['uniqueid'] = $user->getId();
   $userJs['name'] = $user->getPseudo();
   $userJs['email'] = $user->getMail();
   $userJs['photourl'] = $user->getPicture();
}
// 4. Generate the jsConnect string.
// This should be true unless you are testing. 
// You can also use a hash name like md5, sha1 etc which must be the name as the connection settings in Vanilla.
$secure = true; 
WriteJsConnect($userJs, $_GET, $clientID, $secret, $secure);


/*

SELECT Name as Pseudo, Password as password, Photo as picture, Email as mail FROM GDN_User WHERE name != "[Utilisateur supprimé]"

*/